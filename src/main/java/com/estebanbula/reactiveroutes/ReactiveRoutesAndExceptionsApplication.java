package com.estebanbula.reactiveroutes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveRoutesAndExceptionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveRoutesAndExceptionsApplication.class, args);
	}

}
