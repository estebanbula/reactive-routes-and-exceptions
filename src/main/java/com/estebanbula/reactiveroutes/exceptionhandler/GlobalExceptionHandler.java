package com.estebanbula.reactiveroutes.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import reactor.core.publisher.Mono;

import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Mono<String>> handleNoSuchElementException(NoSuchElementException exception) {
        return new ResponseEntity<>(Mono.just(exception.getMessage()), HttpStatus.NOT_FOUND);
    }
}
