package com.estebanbula.reactiveroutes.handler;

import com.estebanbula.reactiveroutes.repository.ToDoRepository;
import com.estebanbula.reactiveroutes.repository.model.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.NoSuchElementException;

@Component
public class ToDoHandler {

    @Autowired
    private ToDoRepository repository;

    public Mono<ServerResponse> retrieveTasks(ServerRequest serverRequest) {
        return ServerResponse.ok().body(repository.findAll(), ToDo.class);
    }

    public Mono<ServerResponse> saveTask(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(ToDo.class)
                .flatMap(repository::save)
                .flatMap(ServerResponse.status(HttpStatus.CREATED)::bodyValue);
    }

    public Mono<ServerResponse> updateTask(ServerRequest serverRequest) {
        String taskId = serverRequest.pathVariable("taskId");
        return serverRequest.bodyToMono(ToDo.class)
                .flatMap(task -> repository.findById(taskId)
                        .flatMap(currentTask -> {
                            currentTask.setTask(task.getTask());
                            return repository.save(currentTask);
                        })
                        .switchIfEmpty(Mono.error(new NoSuchElementException("The task doesn't exist."))))
                .flatMap(ServerResponse.status(HttpStatus.OK)::bodyValue);
    }

    public Mono<ServerResponse> deleteTask(ServerRequest serverRequest) {
        String taskId = serverRequest.pathVariable("taskId");
        return serverRequest.bodyToMono(ToDo.class)
                .flatMap(task -> repository.findById(taskId)
                        .flatMap(repository::delete)
                        .switchIfEmpty(Mono.error(new NoSuchElementException("The task doesn't exist."))))
                .flatMap(ServerResponse.status(HttpStatus.NO_CONTENT)::bodyValue);
    }

    public Mono<ServerResponse> greetings(ServerRequest serverRequest) {
        return ServerResponse.ok().bodyValue("Hello World");
    }
}
