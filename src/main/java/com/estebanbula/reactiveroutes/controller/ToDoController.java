package com.estebanbula.reactiveroutes.controller;

import com.estebanbula.reactiveroutes.repository.model.ToDo;
import com.estebanbula.reactiveroutes.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/toDo")
public class ToDoController {

    @Autowired
    private ToDoService service;


    @GetMapping("/helloWorld")
    public String helloWorld(){
        return service.saludar();
    }

    @PostMapping("/create/task/{task}")
    public Mono<ToDo> createToDo(@PathVariable("task") String task){
        return service.addTask(task);
    }

    @GetMapping("/get/all")
    public Flux<ToDo> getAllTasks(){
        return service.getTasks();
    }

    @PutMapping("update/task/{id}/{newTask}")
    public Mono<ResponseEntity<ToDo>> updateTask(@PathVariable("id") String id, @PathVariable("newTask") String newTask){
        return service.updateTask(id, newTask)
                .map(todo -> new ResponseEntity<>(todo, HttpStatus.OK))
                .onErrorResume(error -> {
                    System.out.println(error.getMessage());
                    return Mono.just(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
                });
    }
}
