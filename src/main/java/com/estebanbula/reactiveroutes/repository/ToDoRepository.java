package com.estebanbula.reactiveroutes.repository;

import com.estebanbula.reactiveroutes.repository.model.ToDo;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ToDoRepository extends ReactiveMongoRepository<ToDo, String> {

}
