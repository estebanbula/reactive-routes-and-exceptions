package com.estebanbula.reactiveroutes.routes;

import com.estebanbula.reactiveroutes.handler.ToDoHandler;
import com.estebanbula.reactiveroutes.service.ToDoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class TaskRoutes {

    @Bean
    public RouterFunction<ServerResponse> routes(ToDoHandler toDoHandler) {
        return nest(path("/api/toDo"),
                route()
                        .GET("", toDoHandler::retrieveTasks)
                        .POST("", toDoHandler::saveTask)
                        .PUT("{taskId}", toDoHandler::updateTask)
                        .DELETE("{taskId}", toDoHandler::deleteTask)
                        .GET("greetings", toDoHandler::greetings)
                        .build());
    }
}
